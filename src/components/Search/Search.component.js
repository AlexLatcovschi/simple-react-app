import React from 'react';
import {DATASET} from '../../json-data/SearchDataSet';
import {DatasetList} from '../DatasetList/DatasetList.component';
import {DebounceInput} from 'react-debounce-input';
import './Search.css';

let filteredDataset = DATASET;
let globalInputValue = '';

export class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data: DATASET};
        const searchUrlParam = window.location.pathname.split('/')[1];
        if (searchUrlParam) {
            globalInputValue = searchUrlParam;
            setTimeout(() => {
                this.searchResultsWithDebounce(searchUrlParam)
            }, 100)
        }
        this.handleChange = this.handleChange.bind(this);
    }

    navigate = (url) => {
        window.history.pushState({"html": {}, "pageTitle": ""}, "search-term", `/${url}`);
    }

    searchResultsWithDebounce(inputValue) {
        filteredDataset = DATASET.filter(el => el.toLowerCase().includes(inputValue.toLowerCase()));
        this.handleChange(filteredDataset);
        this.navigate(inputValue);
    }

    handleChange(event) {
        this.setState({data: event});
    }

    render() {
        return (
            <div>
                <DebounceInput
                    className={'debounce-input'}
                    minLength={2}
                    value={globalInputValue}
                    debounceTimeout={300}
                    onChange={event => this.searchResultsWithDebounce(event.target.value)}/>

                <DatasetList data={this.state.data}></DatasetList>
            </div>
        );
    }
}
