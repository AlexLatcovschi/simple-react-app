import React, {useState} from 'react';
import './DatasetList.css';
import styled from '@emotion/styled'

const NoItemsFound = styled('span')`
  color: red;
  & > a {
    color: red;
  }
`
export class DatasetList extends React.Component {

    constructor(props) {
        super(props);
        this.renderDataSetList(props.data)
    }

    renderDataSetList(dataList) {
        const items = [];
        dataList.map((el, index) => {
            items.push(`<div class='item' key=${index}>${el}</div>`)
        })

        if (document.getElementById('data-list')) {
            document.getElementById('data-list').innerHTML = items.join('');
        }
        return items.join('');
    }

    render() {
        return (<div className={'data-list'}>
            <span className={'data-list'}><NoItemsFound>'Write something in input box!'</NoItemsFound></span>
            <div className={'data-list'} id={'data-list'} dangerouslySetInnerHTML={{ __html: this.renderDataSetList(this.props.data)}}></div>
        </div>);
    }
}
