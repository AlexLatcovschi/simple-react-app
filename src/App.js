import './App.css';
import {Search} from './components/Search/Search.component';

function App() {
  return (
    <div>
        <Search></Search>
    </div>
  );
}

export default App;
